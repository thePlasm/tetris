# tetris

A simple tetris clone written in C with ncurses as an attempt to try and learn how to use ncurses.

## Build

The BSD stdlib and ncurses are required.

	cc -lbsd -lncurses tetris.c -o tetris

For BSDs (including macOS), `-lbsd` is optional.
